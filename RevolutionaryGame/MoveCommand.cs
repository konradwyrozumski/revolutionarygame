﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame
{
    public static class MoveCommand
    {
        public enum Command { MoveForward = 0, TurnLeft = 1, TurnRight = 2 };
    }
}
