﻿using System;
namespace RevolutionaryGame.Interfaces
{
    interface ICommandInvoker
    {
        ICommand GetCommand(MoveCommand.Command command);
    }
}
