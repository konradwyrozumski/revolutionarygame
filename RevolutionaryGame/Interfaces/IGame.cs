﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame.Interfaces
{
    public interface IGame
    {
        bool AddPlayerToGame(IPlayer player);
        bool RemovePlayerFromGame(IPlayer player);
        bool MakeMove(IPlayer player, MoveCommand.Command command);
    }
}
