﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame.Interfaces
{
    public interface ICommand
    {
        MoveCommand.Command Command { get; }
        Position Run(Position currentPosition);
    }
}
