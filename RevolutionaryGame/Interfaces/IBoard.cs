﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame.Interfaces
{
    public interface IBoard
    {
        Position GetStartPosition();
        bool PositionFitsInBoard(Position currentPosition);
    }
}
