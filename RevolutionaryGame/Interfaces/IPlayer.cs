﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame.Interfaces
{
    public interface IPlayer
    {
        Position Position { get; set; }
        IGame Game { get; }

        bool JoinGame(IGame game);
        bool MakeMove(MoveCommand.Command command);
    }
}
