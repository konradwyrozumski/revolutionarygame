﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame.Interfaces
{
    public interface IGameRules
    {
        bool CanMakeMove(Position nextPosition);
        void ResetPlayerPosition(IPlayer player);
        Position GetNextPosition(Position currentPosition, MoveCommand.Command command);
    }
}
