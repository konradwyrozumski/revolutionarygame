﻿using RevolutionaryGame.Command;
using RevolutionaryGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame
{
    public class GameRules : IGameRules
    {
        CommandInvoker commandInvoker;
        IBoard board;
        public GameRules(CommandInvoker commandInvoker, IBoard board)
        {
            this.commandInvoker = commandInvoker;
            this.board = board;

        }

        public void ResetPlayerPosition(IPlayer player)
        {
            player.Position = board.GetStartPosition();
        }

        public bool CanMakeMove(Position nextPosition)
        {            
            if (board.PositionFitsInBoard(nextPosition))
            {                
                return true;
            }
            else
            {
                return false;
            }            
        }

        public Position GetNextPosition(Position currentPosition, MoveCommand.Command command)
        {
            var cmd = commandInvoker.GetCommand(command);
            if (cmd != null)
            {
                var nextPosition = cmd.Run(currentPosition);
                return nextPosition;
            }
            return currentPosition;
        }
    }
}
