﻿using RevolutionaryGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame
{
    public enum Direction { North = 0, East = 1, South = 2, West = 3 };

    public class Position
    {
        public Position()
        {

        }

        public Position(int x, int y, Direction direction)
        {
            this.X = x;
            this.Y = y;
            this.Direction = direction;
        }
        public Position(Position p)
        {
            this.X = p.X;
            this.Y = p.Y;
            this.Direction = p.Direction;
        }

        public int X { get; set; }
        public int Y { get; set; }
        public Direction Direction { get; set; }

        public override bool Equals(object position)
        {
            if (position == null)
            {
                return false;
            }
            Position p = position as Position;
            if (p == null)
            {
                return false;
            }
            return (this.X == p.X) && (this.Y == p.Y) && (this.Direction == p.Direction);
        }
    }

    public class Player : IPlayer
    {
        public Position Position { get; set; }

        private IGame _game;
        public IGame Game
        {
            get { return _game; }
            private set { _game = value; }
        }

        public bool JoinGame(IGame game)
        {
            if (game.AddPlayerToGame(this))
            {
                Game = game;
                return true;
            }
            else
            {
                return false;
            }
        }



        public bool MakeMove(MoveCommand.Command command)
        {
            if (Game == null)
            {
                return false;
            }
            else
            {
                return Game.MakeMove(this, command);
            }
        }
    }
}
