﻿using RevolutionaryGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame.Command
{
    public class CommandInvoker : ICommandInvoker
    {
        List<ICommand> commandList = new List<ICommand>();
        public CommandInvoker()
        {
            LoadCommands();
        }

        void LoadCommands()
        {
            commandList.Add(new MoveForwardCommand());
            commandList.Add(new TurnLeftCommand());
            commandList.Add(new TurnRightCommand());
        }

        public ICommand GetCommand(MoveCommand.Command command)
        {
            foreach (var cmd in commandList)
            {
                if (cmd.Command == command)
                {
                    return cmd;
                }
            }
            return null;
        }
    }
}
