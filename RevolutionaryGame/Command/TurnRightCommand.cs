﻿using RevolutionaryGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame.Command
{
    class TurnRightCommand : ICommand
    {
        public Position Run(Position currentPosition)
        {
            switch (currentPosition.Direction)
            {
                case Direction.North:
                    currentPosition.Direction = Direction.East;
                    break;
                case Direction.South:
                    currentPosition.Direction = Direction.West;
                    break;
                case Direction.East:
                    currentPosition.Direction = Direction.South;
                    break;
                case Direction.West:
                    currentPosition.Direction = Direction.North;
                    break;
            }
            return currentPosition;
        }

        public MoveCommand.Command Command
        {
            get { return MoveCommand.Command.TurnRight; }
        }
    }
}
