﻿using RevolutionaryGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame.Command
{
    class TurnLeftCommand : ICommand
    {
       public Position Run(Position currentPosition)
        {
            switch (currentPosition.Direction)
            {
                case Direction.North:
                    currentPosition.Direction = Direction.West;
                    break;
                case Direction.South:
                    currentPosition.Direction = Direction.East;
                    break;
                case Direction.East:
                    currentPosition.Direction = Direction.North;
                    break;
                case Direction.West:
                    currentPosition.Direction = Direction.South;
                    break;
            }
            return currentPosition;
        }

        public MoveCommand.Command Command
        {
            get { return MoveCommand.Command.TurnLeft; }
        }
    }
}
