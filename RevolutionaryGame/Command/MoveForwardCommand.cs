﻿using RevolutionaryGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame.Command
{
    class MoveForwardCommand : ICommand
    {     

        public Position Run(Position currentPosition)
        {
            switch (currentPosition.Direction)
            {
                case Direction.North:
                    currentPosition.Y += 1;
                    break;
                case Direction.South:
                    currentPosition.Y -= 1;
                    break;
                case Direction.East:
                    currentPosition.X -= 1;
                    break;
                case Direction.West:
                    currentPosition.X += 1;
                    break;
            }
            return currentPosition;
        }

        public MoveCommand.Command Command
        {
            get { return MoveCommand.Command.MoveForward; }
            
        }
    }
}
