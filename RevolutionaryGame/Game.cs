﻿using RevolutionaryGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame
{
    public class Game : IGame
    {
        public List<IPlayer> players { get; private set; }
        public IBoard board { get; private set; }
        public IGameRules gameRules { get; private set; }

        public Game(IBoard board, IGameRules gameRules)
        {
            this.board = board;
            this.gameRules = gameRules;
            this.players = new List<IPlayer>();
        }

        public bool AddPlayerToGame(IPlayer player)
        {
            if (players.Contains(player))
            {
                return false;
            }
            else
            {
                players.Add(player);
                gameRules.ResetPlayerPosition(player);
                return true;
            }
        }

        public bool RemovePlayerFromGame(IPlayer player)
        {
            if (!players.Contains(player))
            {
                return false;
            }
            else
            {
                return players.Remove(player);
            }
        }

        private void ResetAllPlayersPositions()
        {
            foreach (var player in players)
            {
                gameRules.ResetPlayerPosition(player);
            }
        }

        public bool MakeMove(IPlayer player, MoveCommand.Command command)
        {
            var positionCopy = new Position(player.Position);
            var nextPosition = gameRules.GetNextPosition(positionCopy, command);
            if (gameRules.CanMakeMove(nextPosition))
            {
                player.Position = nextPosition;
                return true;
            }
            return false;
        }
    }
}
