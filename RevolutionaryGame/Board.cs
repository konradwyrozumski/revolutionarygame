﻿using RevolutionaryGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame
{
    public class Board : IBoard
    {
        readonly int boardSize;

        public Board(int boardSize)
        {
            this.boardSize = boardSize;
        }

        public bool PositionFitsInBoard(Position currentPosition)
        {
            if (currentPosition.X >= 0 && currentPosition.X <= boardSize &&
                currentPosition.Y >= 0 && currentPosition.Y <= boardSize)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Position GetStartPosition()
        {
            return new Position(0, 0, Direction.North);
        }
    }
}
