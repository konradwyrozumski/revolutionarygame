﻿using Moq;
using NUnit.Framework;
using RevolutionaryGame;
using RevolutionaryGame.Command;
using RevolutionaryGame.Interfaces;
using System;

namespace RevolutionaryGameTests
{
    [TestFixture]
    public class GameRulesTests
    {
        [TestCase]
        public void GameRulesTestResettingPlayerPosition()
        {
            var boardMock = new Mock<IBoard>();
            var player = new Player();

            var startupPosition = new Position(0, 0, Direction.North);
            boardMock.Setup(x => x.GetStartPosition()).Returns(startupPosition);
            var gameRules = new GameRules(null, boardMock.Object);
            gameRules.ResetPlayerPosition(player);
            Assert.AreEqual(startupPosition, player.Position);
        }
    }
}
