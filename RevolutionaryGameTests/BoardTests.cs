﻿using System;
using NUnit.Framework;
using RevolutionaryGame;
using System.Collections.Generic;

namespace RevolutionaryGameTests
{
    [TestFixture]
    public class BoardTests
    {
        IEnumerable<TestCaseData> PositionFitsInBoardTestSource
        {
            get
            {
                yield return new TestCaseData(4, new Position(0, 0, Direction.North), true).SetName("PositionFitsInBoardTest LeftDownCorner");
                yield return new TestCaseData(4, new Position(4, 4, Direction.North), true).SetName("PositionFitsInBoardTest RightUpCorner");
                yield return new TestCaseData(4, new Position(0, 4, Direction.North), true).SetName("PositionFitsInBoardTest RightDownCorner");
                yield return new TestCaseData(4, new Position(4, 0, Direction.North), true).SetName("PositionFitsInBoardTest LeftUpCorner");
                yield return new TestCaseData(4, new Position(2, 2, Direction.North), true).SetName("PositionFitsInBoardTest Middle");
                yield return new TestCaseData(4, new Position(-1, 0, Direction.North), false).SetName("PositionFitsInBoardTest BesidesBoardFromLeft");
                yield return new TestCaseData(4, new Position(0, -1, Direction.North), false).SetName("PositionFitsInBoardTest BesidesBoardFromDown");
                yield return new TestCaseData(4, new Position(5, 0, Direction.North), false).SetName("PositionFitsInBoardTest BesidesBoardFromRight");
                yield return new TestCaseData(4, new Position(0, 5, Direction.North), false).SetName("PositionFitsInBoardTest BesidesBoardFromTop");
            }
        }

        [TestCaseSource(sourceName: "PositionFitsInBoardTestSource")]
        public void PositionFitsInBoardTests(int boardSize, Position position, bool expected)
        {            
            var board = new Board(boardSize);
            var actual = board.PositionFitsInBoard(position);
            Assert.AreEqual(expected, actual);
        }
    }
}
