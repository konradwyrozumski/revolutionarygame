﻿using System;
using NUnit.Framework;
using RevolutionaryGame;
using RevolutionaryGame.Command;
using System.Collections.Generic;

namespace RevolutionaryGameTests
{
    [TestFixture]
    public class CommandInvokerTests
    {
        [TestCase(MoveCommand.Command.MoveForward)]
        [TestCase(MoveCommand.Command.TurnLeft)]
        [TestCase(MoveCommand.Command.TurnRight)]
        public void CommandInvokerGetsCommandTests(MoveCommand.Command command)
        {
            var commandInvoker = new CommandInvoker();
            var cmd = commandInvoker.GetCommand(command);
            Assert.IsNotNull(cmd);
        }

        IEnumerable<TestCaseData> CommandInvokerCommandExecuteTestSource
        {
            get
            {
                yield return new TestCaseData(MoveCommand.Command.MoveForward, new Position(0, 0, Direction.North), new Position(0, 1, Direction.North)).SetName("MoveForward");
                yield return new TestCaseData(MoveCommand.Command.TurnLeft, new Position(0, 0, Direction.North), new Position(0, 0, Direction.West)).SetName("TurnLeft");
                yield return new TestCaseData(MoveCommand.Command.TurnRight, new Position(0, 0, Direction.North), new Position(0, 0, Direction.East)).SetName("TurnRight");
            }
        }
        [TestCaseSource(sourceName: "CommandInvokerCommandExecuteTestSource")]
        public void CommandInvokerCommandExecuteTests(MoveCommand.Command command, Position currentPosition, Position expected)
        {
            var commandInvoker = new CommandInvoker();
            var cmd = commandInvoker.GetCommand(command);
            var nextPosition = cmd.Run(currentPosition);
            Assert.AreEqual(expected, nextPosition);
        }
    }
}
