﻿using Autofac;
using RevolutionaryGame.Command;
using RevolutionaryGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RevolutionaryGame.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = Bootstrap();    
            var player1 = container.Resolve<IPlayer>();
            var game = container.Resolve<IGame>();
            bool joined = player1.JoinGame(game);
            player1.MakeMove(MoveCommand.Command.MoveForward);
            player1.MakeMove(MoveCommand.Command.MoveForward);
            player1.MakeMove(MoveCommand.Command.MoveForward);
            player1.MakeMove(MoveCommand.Command.MoveForward);
            player1.MakeMove(MoveCommand.Command.MoveForward);
            player1.MakeMove(MoveCommand.Command.MoveForward);
            player1.MakeMove(MoveCommand.Command.TurnLeft);
            player1.MakeMove(MoveCommand.Command.TurnLeft);
        }

        private static IContainer Bootstrap()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Game>().AsImplementedInterfaces();
            builder.RegisterType<GameRules>().AsImplementedInterfaces();
            builder.RegisterType<Player>().AsImplementedInterfaces();
            builder.RegisterType<Board>().AsImplementedInterfaces().WithParameter("boardSize", 5);
            builder.RegisterType<CommandInvoker>();
            return builder.Build();
        }
    }
}
